import React from 'react';

import WizardForm from 'containers/form/WizardForm';

export default class Pengajuan extends React.PureComponent {
  render() {
    return (
      <div key="Private" className="app__private app__route">
        <div className="app__container">
          <div className="app__private__content">
            <div className="app__private__intro">
              <h5>Here's Redux Form</h5>
              <small className="text-muted"><i>*Just to have some requests in the sagas...</i></small>
            </div>
            <WizardForm />
          </div>
        </div>
      </div>
    );
  }
}
