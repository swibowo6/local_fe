import React from 'react';

import MultiStepWizard from 'containers/wizard/MultiStepWizard';

export class Formik extends React.PureComponent {
  render() {
    return (
      <main key="Home" className="app__home app__route">
        <MultiStepWizard />
      </main>
    );
  }
}


export default Formik;
