import React from 'react';
import PropTypes from 'prop-types';
import { Formik } from 'formik';
import { Debug } from './debug';

class Wizard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      values: props.initialValues,
    };
  }

  static Page = ({ children }) => children;

  next = values => this.setState(state => ({
    // eslint-disable-next-line
    page: Math.min(state.page + 1, this.props.children.length - 1),
    values,
  }));

  previous = () => this.setState(state => ({
    page: Math.max(state.page - 1, 0),
  }));

  validate = values => {
    // eslint-disable-next-line
    const activePage = React.Children.toArray(this.props.children)[
    // eslint-disable-next-line
      this.state.page
    ];
    return activePage.props.validate ? activePage.props.validate(values) : {};
  };
  // eslint-disable-next-line
  handleSubmit = (values, bag) => {
    const { children, onSubmit } = this.props;
    const { page } = this.state;
    const isLastPage = page === React.Children.count(children) - 1;
    if (isLastPage) {
      return onSubmit(values, bag);
    }
    this.next(values);
    bag.setTouched({});
    bag.setSubmitting(false);
  };

  render() {
    const { children } = this.props;
    const { page, values } = this.state;
    const activePage = React.Children.toArray(children)[page];
    const isLastPage = page === React.Children.count(children) - 1;
    return (
      <Formik
        initialValues={values}
        enableReinitialize={false}
        validate={this.validate}
        onSubmit={this.handleSubmit}
        // eslint-disable-next-line
        render={({ values, handleSubmit, isSubmitting, handleReset }) => (
          <form onSubmit={handleSubmit}>
            {activePage}
            <div className="buttons">
              {page > 0 && (
                <button
                  type="button"
                  className="secondary"
                  onClick={this.previous}
                >
                  « Previous
                </button>
              )}

              {!isLastPage && <button type="submit">Next »</button>}
              {isLastPage && (
                <button type="submit" disabled={isSubmitting}>
                  Submit
                </button>
              )}
            </div>
            <Debug />
          </form>
        )}
      />
    );
  }
}
Wizard.propTypes = {
  children: PropTypes.any,
  initialValues: PropTypes.any,
  onSubmit: PropTypes.func,
};

export default Wizard;
