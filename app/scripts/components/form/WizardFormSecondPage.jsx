import React from 'react';
import { Field, reduxForm } from 'redux-form';
import validate from 'containers/form/validate';
import renderField from 'containers/form/renderField';
import PropTypes from 'prop-types';

const renderError = ({ meta: { touched, error } }) => (touched && error ? <span>{error}</span> : false);

const WizardFormSecondPage = props => {
  const { handleSubmit, previousPage } = props;
  return (
    <form onSubmit={handleSubmit}>
      <Field name="email" type="email" component={renderField} label="Email" />
      <div>
        <label // eslint-disable-line
        >Sex
        </label>
        <div>
          <label htmlFor>
            <Field
              name="sex"
              component="input"
              type="radio"
              value="male"
            />{' '}
            Male
          </label>
          <label // eslint-disable-line
          >
            <Field
              name="sex"
              component="input"
              type="radio"
              value="female"
            />{' '}
            Female
          </label>
          <Field name="sex" component={renderError} />
        </div>
      </div>
      <div>
        <button type="button" className="previous" onClick={previousPage}>
          Previous
        </button>
        <button type="submit" className="next">
          Next
        </button>
      </div>
    </form>
  );
};

WizardFormSecondPage.propTypes = {
  handleSubmit: PropTypes.func,
  previousPage: PropTypes.func,
};
renderError.propTypes = {
  meta: PropTypes.object,
};

export default reduxForm({
  form: 'wizard', // Form name is same
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
  validate,
})(WizardFormSecondPage);
