import React, { Component } from 'react';
import PropTypes from 'prop-types';
import WizardFormFirstPage from 'components/form/WizardFormFirstPage';
import WizardFormSecondPage from 'components/form/WizardFormSecondPage';
import WizardFormThirdPage from 'components/form/WizardFormThirdPage';

class WizardForm extends Component {
  constructor(props) {
    super(props);
    this.nextPage = this.nextPage.bind(this);
    this.previousPage = this.previousPage.bind(this);
    this.state = {
      page: 1,
    };
  }

  nextPage() {
    // eslint-disable-next-line
    this.setState({ page: this.state.page + 1 });
  }

  previousPage() {
    // eslint-disable-next-line
    this.setState({ page: this.state.page - 1 });
  }

  render() {
    const { onSubmit } = this.props;
    const { page } = this.state;
    return (
      <div>
        {page === 1 && <WizardFormFirstPage onSubmit={this.nextPage} />}
        {page === 2 && (
          <WizardFormSecondPage
            previousPage={this.previousPage}
            onSubmit={this.nextPage}
          />
        )}
        {page === 3 && (
          <WizardFormThirdPage
            previousPage={this.previousPage}
            onSubmit={onSubmit}
          />
        )}
      </div>
    );
  }
}

WizardForm.propTypes = {
  onSubmit: PropTypes.any.isRequired,
};

export default WizardForm;
