import React from 'react';
import { Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import Wizard from 'components/wizard/Wizard';

import '../../../styles/containers/_form.scss';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

const required = value => (value ? undefined : 'Required');

const testSchema = Yup.object().shape({
  testField: Yup.string()
    .min(2, 'Must be longer than 2 characters')
    .max(20, 'Nice try, nobody has a last name that long')
    .required('Required'),
});

// const validationField

const MultiStepWizard = () => (
  <div className="_form_style">
    <h3>Form Pengajuan Pinjaman</h3>
    <Wizard
      initialValues={{
        firstName: '',
        lastName: '',
        email: '',
        favoriteColor: '',
        anotherMail: '',
        phoneNumber: '',
        testField: '',
      }}
      onSubmit={(values, actions) => {
        sleep(300).then(() => {
          // eslint-disable-next-line
            // window.alert(JSON.stringify(values, null, 2));
          console.log(values);
          actions.setSubmitting(false);
        });
      }}
    >
      <Wizard.Page>
        <div>
          <Field
            name="firstName"
            component="input"
            type="text"
            placeholder="First Name"
            validate={required}
            className="_field_style"
          />
          <ErrorMessage name="firstName">
            {(msg) => (
              <div className="alert alert-danger"> {msg} </div>
            )}
          </ErrorMessage>
        </div>
        <div>
          <Field
            name="lastName"
            component="input"
            type="text"
            placeholder="Last Name"
            validate={required}
            className="_field_style"
          />
          <ErrorMessage name="lastName">
            {(msg) => (
              <div className="alert alert-danger"> {msg} </div>
            )}
          </ErrorMessage>
        </div>
      </Wizard.Page>
      <Wizard.Page
        validate={values => {
          const errors = {};
          if (!values.email) {
            errors.email = 'Required';
          }
          if (!values.favoriteColor) {
            errors.favoriteColor = 'Required';
          }
          // console.log(errors);
          return errors;
        }}
      >
        <div>
          <Field
            name="email"
            component="input"
            type="email"
            placeholder="Email"
            className="_field_style"
          />
          <ErrorMessage name="email">
            {(msg) => (
              <div className="alert alert-danger"> {msg} </div>
            )}
          </ErrorMessage>
        </div>
        <div>
          <Field name="favoriteColor" component="select" className="_field_style">
            <option value="">Select a Color</option>
            <option value="#ff0000"> Red</option>
            <option value="#00ff00"> Green</option>
            <option value="#0000ff"> Blue</option>
          </Field>
          <ErrorMessage name="favoriteColor">
            {(msg) => (
              <div className="alert alert-danger"> {msg} </div>
            )}
          </ErrorMessage>
        </div>
      </Wizard.Page>
      {/* <Wizard.Page
        validate={values => {
          const errors = {};
          if (!values.anotherMail) {
            errors.anotherMail = 'Required Field';
          }
          else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.anotherMail)) {
            errors.anotherMail = 'Invalid email address';
          }
          if (!values.phoneNumber) {
            errors.phoneNumber = 'Required Field';
          }
          else if (!/^\d{11,13}$/i.test(values.phoneNumber)) {
            errors.phoneNumber = 'Phone Number Must be longer than 12';
          }
          return errors;
        }}
      >
        <div>
          <Field name="anotherMail" placeholder="example@mail.com" component="input" className="_field_style" />
          <ErrorMessage name="anotherMail">
            {(msg) => (
              <div className="alert alert-danger"> {msg} </div>
            )}
          </ErrorMessage>
        </div>
        <div>
          <Field name="phoneNumber" placeholder="0852136485126" component="input" className="_field_style" />
          <ErrorMessage name="phoneNumber>
            {(msg) => (
              <div className="alert alert-danger"> {msg} </div>
            )}
          </ErrorMessage>
        </div>
      </Wizard.Page> */}
      <Wizard.Page
        // validate={values => {
        //   return sleep(500).then(() => {
        //     const errors = {};
        //     if ([''].includes(values.testField)) {
        //       errors.testField = 'Required';
        //     }
        //     testSchema.validate({ testField: values.testField })
        //       .catch((err) => {
        //         if (err) {
        //           errors.testField = 'Reqired';
        //         }
        //       });
        //     if (Object.keys(errors).length) {
        //       throw errors;
        //     }
        //   });
        // }}
      >
        <Field name="testField" validate={testSchema} placeholder="test" component="input" className="_field_style" />
        <ErrorMessage name="testField">
          {(msg) => (
            <div className="alert alert-danger"> {msg} </div>
          )}
        </ErrorMessage>
      </Wizard.Page>
    </Wizard>
  </div>
);

export default MultiStepWizard;
