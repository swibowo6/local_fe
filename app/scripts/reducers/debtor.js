import { handleActions } from 'redux-actions';
import immutable from 'immutability-helper';

import { ActionTypes, STATUS } from 'constants/index';

export const debtorState = {
  status: STATUS.IDLE,
  statusPengajuan: 'online',
};


export default {
  user: handleActions({
    [ActionTypes.DEBTOR_REQUEST]: (state) => immutable(state, {
      status: { $set: STATUS.RUNNING },
    }),
    [ActionTypes.DEBTOR_SUCCESS]: (state) => immutable(state, {
      statusPengajuan: { $set: 'online' },
      status: { $set: STATUS.READY },
    }),
    [ActionTypes.DEBTOR_FAILURE]: (state) => immutable(state, {
    }),
  }, debtorState),
};
