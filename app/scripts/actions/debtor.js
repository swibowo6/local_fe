// @flow
/**
 * @module Actions/Debtor
 * @desc Debtor Actions
 */
import { createActions } from 'redux-actions';

import { ActionTypes } from 'constants/index';

export const {
  debtorPost: post,
} = createActions({
  [ActionTypes.DEBTOR_REQUEST]: () => ({}),
});
